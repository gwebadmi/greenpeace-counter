# Greenpeace Counter API

Below are the JavaScript functions we use to request action/reponse

These probably are best inserted just before the end of the body tag ```</body>```
 
Be sure to set the target element to display the counter inside\
e.g. we are using document.getElementById("counterAnimation")\
But you can change this to suit

# Display the counter (including animation)
```
<script>
    let	getCountURL = "https://counter.greenpeace.org/signups?id=global.climate.shift.petition";
    async function getCounter() {
        const response = await fetch(getCountURL);
        const counter_response = await response.json();
        let count = counter_response.unique_count;
        let counter = count/2; // start animation at 50% of total

        function animateVal(targetElement, start, end, steps, duration) {   
            start = Math.round(counter);
            let stepsize = (end - start) / steps
            let current = start
            var stepTime = Math.abs(Math.floor(duration / (end - start)));
            let stepspassed = 0
            let stepsneeded = (end - start) / stepsize
            let x = setInterval( () => {
                    current += stepsize
                    stepspassed++
                    targetElement.innerHTML = Math.round(current)
                if (stepspassed >= stepsneeded) {
                    clearInterval(x)
                }
            }, stepTime)
        }   
        // animateVal(targetElement, start, end, steps, duration)
        animateVal(document.getElementById("counterAnimation"), counter, count, counter, 5000);
    }
    getCounter();
</script>
```
## Increment/update the counter after form submitted

When a request or ping is made to the api enpoint the counter will increment by 1.
This is the default response - however you will not receive any data in the response.
You can confirm by checking the total using the [link](https://counter.greenpeace.org/signups?id=global.climate.shift.petition) refernced above.

```
<script>
    // increment counter - request counter endpoint

    let incrementCounterURL = "https://counter.greenpeace.org/count?id=global.climate.shift.petition";

      fetch(incrementCounterURL)
      .then(response => {
          console.log("response",response);
      })
      .catch(error => {
          console.log("error ",error);
      });
</script>
```
